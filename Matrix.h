#pragma once

class Matrix
{
public:
	//deklaracja publicznych zmiennych
	int rows, cols;
	int **body; //wskaznik do tablicy wskaznikow

	//deklaracja konstruktorow
	Matrix(void);
	Matrix(Matrix& matrixModel);
	Matrix(int _rows, int _cols);
	
	~Matrix(void); //deklaracja destruktora

	//deklaracja przeciazanych operatorow
	friend ostream & operator<< (ostream &output, const Matrix &mat);
	Matrix operator* (int number);
	Matrix operator+ (Matrix mat);
	int* operator[] (size_t row);
	Matrix operator= (Matrix &mat);

	//deklaracja publicznej metody wypelniajacej macierz
	void Fill(int value);

private:
	//deklaracja prywatnej metody alokujacyej pamiec dla skladnikow obiektu
	void RealocateMemory(int rows, int cols);

};