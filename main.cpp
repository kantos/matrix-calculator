#include "stdafx.h"
#include "Matrix.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//definicje zmiennych
	int rows, cols;
	Matrix matrix, matrix2, matrix3;

	//pobranie z klawiatury wielkosci macierzy
	cout << "Podaj liczbe wierszy: ";
	cin >> rows;
	cout << "Podaj liczbe kolumn: ";
	cin >> cols;
	
	matrix = Matrix(rows, cols); //deklaracja zmiennej
	matrix.Fill(2);			//wypelnienie macierzy dwojkami
	matrix = matrix * 2;	//mnozenie macierzy przez 2
	cout << matrix;			//drukowanie macierzy na ekran
	
	matrix2 = Matrix(rows, cols); //deklaracja zmiennej
	matrix2.Fill(3);		//wypelnienie macierzy trojkami
	cout << matrix2;		//drukowanie macierzy na ekran

	matrix3 = matrix + matrix2;	//sumowanie dwoch macierzy
	cout << matrix3;			//drukowanie macierzy na ekran

	
	cin >> rows; //pobieranie z klawiatury zmiennej w celu zatrzymania wykonywania programu	
	
	return 0;
}

