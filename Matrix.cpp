#include "StdAfx.h"
#include "Matrix.h"

Matrix::Matrix(void)
{
	//ten konstruktor jest wywolywany kiedy deklarujemy
	//zmienna typu Matrix bez definicji
}

Matrix::Matrix(Matrix &matrixModel)
{
	//ten konstruktor jest wywolywany przy definicji podczas deklaracji

	//przepisywanie liczb calkowitych do kopii obiektu
	this->rows = matrixModel.rows;
	this->cols = matrixModel.cols;

	//przypisywanie nowych adresow do kopii obiektu
	this->body = new int*[this->rows];
	for (int i=0; i<this->rows; i++)
	{
		this->body[i] = new int[this->cols];
	}

	//przepisywanie wartosci do nowego obiektu
	for (int i=0; i<this->rows; i++)
	{
		for (int j=0; j<this->cols; j++)
		{
			this->body[i][j] = matrixModel.body[i][j];
		}
	}
}

Matrix Matrix::operator=(Matrix &mat)
{
	//ten konstruktor jest wywolywany przy definicji poza deklaracja
	if(&mat == this) return *this; //sprawdzamy czy nie przepisujemy tego samego obiektu

	//przepisywanie liczb calkowitych do nowego obiektu
	this->rows = mat.rows;
	this->cols = mat.cols;

	//przypisywanie nowych adresow do kopii obiektu
	this->body = new int*[this->rows];
	for (int i=0; i<this->rows; i++)
	{
		this->body[i] = new int[this->cols];
	}

	//przepisywanie wartosci do nowego obiektu
	for(int i=0; i<this->rows; i++)
	{
		for(int j=0; j<this->cols; j++)
		{
			this->body[i][j] = mat.body[i][j];
		}
	}
	return *this;
}

Matrix::Matrix(int _rows, int _cols)
{
	//konstruktor wywolywany przy podaniu wielkosci macierzy
	rows = 0;
	cols = 0;
	body = NULL;
	this->RealocateMemory(_rows, _cols);
}

Matrix::~Matrix(void)
{
	//niszczenie adresow poszegolnych tablic
	for (int i=0; i<rows; i++)
	{
		delete[] body[i];
	}
	delete[] body; //niszczenie adresu tablicy adresow
}

void Matrix::RealocateMemory(int _rows, int _cols)
{
	//przypisywanie zmiennyc liczbowych
	rows = _rows;
	cols = _cols;

	//przypisywanie adresow dla tablicy
	body = new int*[rows];
	for (int i=0; i<rows; i++)
	{
		body[i] = new int[cols];
	}
}

void Matrix::Fill(int value)
{
	//wypelnianie wszystkich pol macierzy zadana waroscia
	for (int i=0; i<rows; i++)
	{
		for (int j=0; j<cols; j++)
		{
			body[i][j] = value;
		}
	}
}

ostream & operator<< (ostream &output, const Matrix &mat)         
{
	//wypisywanie na ekran macierzy
	for (int i=0; i<mat.rows; i++)
	{
		for (int j=0; j<mat.cols; j++)
		{
			output << mat.body[i][j] << " ";
		}
		output << endl;
	}
	return output << endl;
}

Matrix Matrix::operator+(Matrix mat)
{
	//sumowanie macierzy
	//sprawdzanie czy maceirze sa tej samej wielksoci
	if(rows == mat.rows && cols == mat.cols)
	{
		for (int i=0; i<rows; i++)
		{
			for (int j=0; j<cols; j++)
			{
				body[i][j] = body[i][j] + mat.body[i][j];
			}
		}
		return *this;
	}
	else 
	{ 
		//jesli maceirze nie sa takiej samej wielkosci 
		//wyrzucamy blad
		std::string ex = "Wrong matrix size";
		throw ex;
	}
}

Matrix Matrix::operator* (int number)
{
	//mnozenie macierzy przez liczbe
	for (int i=0; i<rows; i++)
	{
		for (int j=0; j<cols; j++)
		{
			this->body[i][j] = this->body[i][j] * number;
		}
	}
	return *this;
}

int* Matrix::operator[] (size_t row)
{
	//operowanie ma obiekcie klasy jak na tablicy
	return body[row];
}
